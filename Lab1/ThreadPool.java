import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class ThreadPool {
	public static boolean isShutDown=false;  //flag to shut down the service
	public static final String DefaultWelcomes="Welcome to the Server"+'\n'+"A.ToUpperCase e.g. A hello"+'\n'+"B.JoinChatroom(not available)"+'\n';
	public static final int ThreadMaxNum=20;
	public static final int ThreadNormalNum=10;
	public static int CurrentThreadNum=10;
	private static List<Socket> SocketQueue=Collections.synchronizedList(new LinkedList<Socket>()); //store request socket
	private LinkedList<Excutor> ExcutorList;  // thread list
	private static ThreadPool Instance;
	private ManageThread Manager;
	
	private ThreadPool(){
		Manager=new ManageThread();
		ExcutorList=new LinkedList<Excutor>();
		for(int i=0;i<ThreadNormalNum;i++){
			this.ExcutorList.add(new Excutor());
		}
	}
	
	public static ThreadPool GetInstance(){            //single pattern
		if(Instance==null){
			Instance=new ThreadPool();
		}
		return Instance;
	}
	
	public void Destory(){
		ExcutorList.notifyAll();
		ExcutorList.clear();
	}
	
	public void Start(){
		for(int i=0;i<this.ExcutorList.size();i++){
			this.ExcutorList.get(i).start();
		}
		Manager.start();
	}
	
//	public Excutor getExcutor(){
//		synchronized(ExcutorList){
//			if(ExcutorList.size()>0){
//				return ExcutorList.removeFirst();
//			}
//			else{
//				if(CurrentThreadNum<=ThreadMaxNum){
//					CurrentThreadNum++;
//					return new Excutor();
//				}
//				else{
//					try{
//						ExcutorList.wait();
//					}
//					catch(InterruptedException e){
//						e.printStackTrace();
//					}
//					return ExcutorList.removeFirst();
//				}
//			}
//		}
//	}
//	public void SetBackExcutor(Excutor T){
//		synchronized(ExcutorList){
//			this.ExcutorList.add(T);
//			ExcutorList.notify();
//			
//		}
//	}
	
	public void AddSocket(Socket sk){
		synchronized(this.SocketQueue){
			this.SocketQueue.add(sk);
			this.SocketQueue.notify();
		}
	}
	
	
	
	private class Excutor extends Thread {
		
		private Socket socket;
		private BufferedReader InFromClient;
		private DataOutputStream OutToClient;
		public void SetSocket(){
			synchronized(SocketQueue){
				if(SocketQueue.size()>0){
					socket= SocketQueue.remove(0);
				}
				else{
					try{
						SocketQueue.wait();
					}
					catch(InterruptedException e){
						e.printStackTrace();
					}
					if(!SocketQueue.isEmpty())
					socket= SocketQueue.remove(0);
				}
			}
		}
		

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(!isShutDown){
				SetSocket();
				
				try{
					OutToClient=new DataOutputStream(socket.getOutputStream());
					InFromClient=new BufferedReader(new InputStreamReader(socket.getInputStream()));
					ShowDefaultHelper();
				}
				catch(IOException e){
					e.printStackTrace();
				}
				while(true){
					String[] contextFromClient=new String[2];
					try {
						contextFromClient = InFromClient.readLine().split("\\s+");
						if(contextFromClient[0].equalsIgnoreCase("KillService")){
							OutToClient.writeBytes("Halo text"+'\n'+"IP:"+socket.getInetAddress().getHostAddress()+'\n'+"Port Num:"+Integer.toString(socket.getLocalPort())+'\n'+"Student ID: 14307009"+'\n');
							Thread.sleep(1000);      //In case buffer
							socket.close();
							break;
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e){
						
					}
					
					switch(contextFromClient[0]){
						case "A": ToUpperCase(contextFromClient[1]);
									break;
						default: FormatErrorHandle();
									break;
					}
				}
			}
			
		}
		
		public void ShowDefaultHelper() throws IOException{
			
			OutToClient.writeBytes(DefaultWelcomes);
			
		}
		
		public void ToUpperCase(String context){
			String modifiedcontext;
			try {
				
				modifiedcontext=context.toUpperCase()+'\n';
				OutToClient.writeBytes(modifiedcontext);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public void FormatErrorHandle(){
			try {
				OutToClient.writeBytes("illegal Format"+'\n');
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	private class ManageThread extends Thread{          //control the Num of Excutor

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			DynamicControl();
		}
		private void DynamicControl(){
			while(!isShutDown){
				if(SocketQueue.size()>ExcutorList.size()&&ExcutorList.size()<=ThreadMaxNum){
					synchronized(ExcutorList){
						Excutor E=new Excutor();
						ExcutorList.addLast(E);
						E.start();
					}
				}
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
