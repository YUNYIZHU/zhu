# README #
* Student ID: 14307009 Student Name: Yunyi Zhu
* Lab1: Multithreaded TCP Server
* Server has a thread pool which contains 10 threads( maxinum is 20 threads) and a manage thread which can manage number of threads in the thread pool.
* Client has two threads. One read message from server and print it, another read command from client and sent message to server.

PS. I never use linux. I don't know how to write .sh file. I try to create a .sh file, but it did not work. I am sorry about that, but I will keep do it on some linux machines. if it works, I will push it to here.

### How to execute? ###

* open one command prompt and enter the root.
* Type javac *.java (complie all java file)
* Type: java Server 6789              (to run server)
* open another command prompt and enter the root
* Type: java ClientLauncher 6789           (to run client)
* Type:"A abc"  (A is a command that allows server know which service you want )
* Then server will send ABC back and client will print it.
* Type:"HELO text"
* Type: KillService              (to terminate connection, client and server)